//DB config
const mongoose = require('mongoose');

const DB = mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI);
mongoose.connection
  .once('open', () => console.log('Connection with DB successfully established'))
  .on('error', (error) => {
    console.warn(`DB connection error: ${error}`);
});

module.exports = DB;
