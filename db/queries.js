const User = require('../models/user'),
  request = require('request'),
  access_token = require('../FBconfig').access_token;

function getUserProfile(sender_psid) {

  request({
    "url": `https://graph.facebook.com/v2.6/${sender_psid}`,
    "qs": {
      "fields": "first_name",
      "access_token": access_token,
    },
    "method": "GET"
  }, (err, res, body) => {
    if (err) {
      console.log(`Error getting user's name: ${error}`);
    } else {
      let bodyObj = JSON.parse(body);
      let name = bodyObj.first_name;
      response = {"text": `Hello ${name}`};
      callSendAPI(sender_psid, response);
    }
  });
}

// Sends response messages via the Send API
function callSendAPI(sender_psid, response) {
  // Constructs the message body
  let request_body = {
    "recipient": {
      "id": sender_psid
    },
    "message": response
  }

  // Sends the HTTP request to the Messenger Platform
  request({
    "uri": "https://graph.facebook.com/v2.6/me/messages",
    "qs": { "access_token": access_token },
    "method": "POST",
    "json": request_body
  }, (err, res, body) => {
    if (!err) {
      console.log('message sent!');
    } else {
      console.error(`Unable to send message: ${err}`);
    }
  });

}

// Queries factory
const DBQueries = () => {
  return {
    // Sends text message back to User and save message in DB
    textMessageSaveAndSend: (sender_psid, response, received_message) => {
      User.findOne({ senderID: sender_psid })
      .then((user) => {
        user.messages.push({ messageID: received_message.mid, messageText: received_message.text })
        user.save();
        response = { "text": `You wrote: ${received_message.text}` };
        callSendAPI(sender_psid, response);
      })
      .catch(() => {
        console.log(`User does not exists, creating new User`);
        getUserProfile(sender_psid);
        User.create({
          senderID: sender_psid,
          messages: [{ messageID: received_message.mid, messageText: received_message.text }]
        })
      .catch(error => console.log(error));
      });
    },

    // Sends back text message on 'last phrase' event
    textMessageOnLastPhrase: (sender_psid, response, received_message) => {
      User.findOne({ senderID: sender_psid })
      .then((user) => {
        response = {"text": `Your last phrase was: ${user.messages[user.messages.length - 1].messageText}`};
        callSendAPI(user.senderID, response);
      })
      /* In case of User does not exists and we type 'last phrase' as the first message to our bot.
      Therefore the first text message for current User will be saved in DB as 'last phrase' and
      bot will return welcome message */
      .catch(() => {
        console.log(`User does not exists, creating new User`)
        User.create({
          senderID: sender_psid,
          messages: [{ messageID: received_message.mid, messageText: received_message.text }]
        })
        .then(() => {
          getUserProfile(sender_psid);
        })
        .catch(error => console.log(error));
      });
    },

    // Sends attachment back to User and save attachment in DB
    attachmentSaveAndSend: (sender_psid, response, received_message, attachment_url, attachment_type) => {
      User.findOne({ senderID: sender_psid })
        .then((user) => {
          user.attachments.push({ messageID: received_message.mid, messageAttachment: attachment_url })
          user.save();
          response = {
            "attachment": {
              "type": attachment_type, 
              "payload": {
                "url": attachment_url, 
                "is_reusable": true
              }
            }
          };
          callSendAPI(sender_psid, response);
        })
        .catch(() => {
          console.log(`User does not exists, creating new User and assign attachment to the newly created User`)
          User.create({
            senderID: sender_psid,
            attachments: [{ messageID: received_message.mid, messageAttachment: attachment_url }]
          })
          .catch(error => console.log(error));
        });
    }
  }
}

module.exports = DBQueries;
