const router = require('express').Router();

// Deployment test
router.get('/', (req, res) => {
  res.send('We are on Heroku!');
});

module.exports = router;
