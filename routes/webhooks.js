const router = require('express').Router(),
  verify_token = require('../FBconfig').verify_token,
  DBQueries = require('../db/queries')();

// Adds support for GET requests to our webhook
router.get('/', (req, res) => {

  let VERIFY_TOKEN = verify_token;
    
  // Parse the query params
  let mode = req.query['hub.mode'];
  let token = req.query['hub.verify_token'];
  let challenge = req.query['hub.challenge'];
    
  if (mode && token) {
    if (mode === 'subscribe' && token === verify_token) {
      console.log('WEBHOOK_VERIFIED');
      res.status(200).send(challenge);
    
    } else {
      res.send('No entry!').sendStatus(403); 
    }
  }
});

// Post route
router.post('/', (req, res) => {
 
  let body = req.body;

  // Checks this is an event from a page subscription
  if (body.object === 'page') {

    body.entry.forEach(function(entry) {

      // Gets the message. entry.messaging is an array, but 
      // will only ever contain one message, so we get index 0
      let webhook_event = entry.messaging[0];
      console.log(webhook_event);

      // Get the sender PSID
      let sender_psid = webhook_event.sender.id;

      // Check if the event is a message
      if (webhook_event.message) {
        handleMessage(sender_psid, webhook_event.message);        
      }
    });

    res.status(200).send('EVENT_RECEIVED');
  } else {
    res.sendStatus(404);
  }
});

// Handles messages events
function handleMessage(sender_psid, received_message) {
  
  let response;

  // Check if the message contains text
  if (received_message.text) {

    switch (received_message.text) {
      case "last phrase":
        DBQueries.textMessageOnLastPhrase(sender_psid, response, received_message);
        break;
      default:
        DBQueries.textMessageSaveAndSend(sender_psid, response, received_message);
    }
  } else if (received_message.attachments) {

    // Gets the URL of the message attachment and attachment type
    let attachment_url = received_message.attachments[0].payload.url;
    let attachment_type = received_message.attachments[0].type;

    DBQueries.attachmentSaveAndSend(sender_psid, response, received_message, attachment_url, attachment_type);
  }
};

module.exports = router;
