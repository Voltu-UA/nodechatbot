Simple chat bot built on top of FB mesenger platform which does the following:

1. After the first time phrase meets the user with the FB account username;
2. Answer to any phrase: You wrote {text} or {file}; 
3. Every messages or files logs to DB;
4. On *last phrase* sends back the last message or file send by the user.