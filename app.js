const express = require('express'),
  bodyParser = require('body-parser'),
  app = express().use(bodyParser.json()),
  DB = require('./db/config');
  
// Routes
const heroku = require('./routes/heroku'),
  webhooks = require('./routes/webhooks');

app.use('/', heroku);
app.use('/webhook', webhooks);

// Sets server port and logs message on success
app.set('port', (process.env.PORT || 5000));
app.listen(app.get('port'), () => console.log(`webhook is listening on port: ${app.get('port')}`));
