const mongoose = require('mongoose'),
  MessageSchema = require('./message'),
  AttachmentSchema = require('./attachment'),
  Schema = mongoose.Schema;

const UserSchema = new Schema({
  senderID: String,
  messages: [MessageSchema],
  attachments: [AttachmentSchema]
});

const User = mongoose.model('user', UserSchema);
module.exports = User;
