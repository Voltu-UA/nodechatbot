const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const AttachmentSchema = new Schema({
  messageID: String,
  messageAttachment: String,
});

module.exports = AttachmentSchema;
