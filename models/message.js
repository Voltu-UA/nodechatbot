const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const MessageSchema = new Schema({
  messageID: String,
  messageText: String,
});

module.exports = MessageSchema;
